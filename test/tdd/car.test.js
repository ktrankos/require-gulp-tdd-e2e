'use strict';

describe('Factory', function(){
  var Car;
  beforeEach(module('myApp'));
  beforeEach(inject(function(_Car_){
    Car = new _Car_('blue');
  }));

  describe('Car', function(){
    it('tiene la propiedad "color"', function(){
      expect(Car).to.have.property('color');
    });
    it('el color que le he asignado es "blue"', function(){
      expect(Car.color).to.equal('blue');
    })
  });
});