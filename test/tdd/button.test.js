'use strict';

describe('Directive', function(){
  var element, scope;
  //al ser una directiva con un template externo 
  beforeEach(module('myApp',
    'directive/button.directive.html'));

  beforeEach(inject(function($compile, $rootScope){    
    //cudidado con la creaccion de scope, en muchos manuales usa $new como variable y es un metodo.
    scope = $rootScope.$new();
    element = $compile('<button-directive data="{name:\'pedro\'}"></button-directive>')(scope);
    scope.$digest();
  }));
  describe('"button.directive"', function(){
    it('Existe el element y scope', function(){
      expect(scope).to.not.be.undefined;
      expect(element).to.not.be.undefined;
    });
    it('scope.data.name + text("hola alvaro")==="hola alvaro pedro"', function(){    
      expect(element.find('p').text()).to.equal('hola alvaro pedro');
    }); 
  });
  
});