describe('Controller :', function(){
  var ctrl, interval;
  beforeEach(angular.mock.module('myApp'));
  beforeEach(inject(function($rootScope, $controller, $interval){        
    var scope = $rootScope.$new();
    interval = $interval;
    ctrl = $controller('demoCtrl', {$scope: scope, $interval: $interval});            
  }));
  describe('"demoCtrl"', function(){
    it('El controller existe', function(){            
      expect(ctrl).to.not.be.undefined;
    });
    it('Acceso a sus values. scope.value === "soy demo"', function(){
      expect(ctrl.value).to.equal('soy demo');
    });
    it('Acceso a un $interval, producciendo un flush()', function(){
      interval.flush(7000);
      assert.isBelow(ctrl.count, 21);
    });
  });
  describe('"demoCtrl::::ats-messages!!!"', function(){
    it('tiene 2 elementos ats-message', function(){
      expect(ctrl.messages).to.have.length(2);
    });
    it('el text del 0 item ==="primero de todos los que estais aqui so male"', function(){
      expect(ctrl.messages).to.have.deep.property('[0].text', 'primero de todos los que estais aqui so male');
    });
    it('message tiene keys: text, duration, type, position', function(){
      expect(ctrl.messages[0]).to.have.all.keys(['text', 'duration', 'type', 'position']);
    });
  });
});