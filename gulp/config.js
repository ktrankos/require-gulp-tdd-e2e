'use strict';

var configuration = function(task){
  var conf = {
    html: {
      files: ['./www/*.html','./www/directive/*.html'],
      src:'./www/*.html'
    },
    bower:{
      files:['./bower_components/angular/angular.js'],
      file: 'bower.js',
      dest: './www/dist/js/'
    },
    connect:{
      root: 'www'
    },
    sass:{
      files: './src/sass/*.scss',
      dest:'./www/css'
    },
    jshint: {
      src:'./src/*/**/*.js',
      reporter: 'default'
    },
    browserify:{
      files: ['./src/app.main.js'],
      file:'app.js',
      dest: './www/js',
      insertGlobals: true,
      debug: true,
      watchFiles:['./src/*/**/*.js']
    }

  };
  return conf[task];
};

module.exports = configuration;
