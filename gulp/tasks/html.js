'use strict';

var gulp = require('gulp'),
  connect = require('gulp-connect'),
  html = require('./../config')('html');


gulp.task('html', function () {
  gulp.src(html.src)
  .pipe(connect.reload());
});