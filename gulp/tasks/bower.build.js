'use strict';

var gulp = require('gulp')
, concat = require('gulp-concat')
, config = require('./config')('bower');

gulp.task('bower', function() {
    gulp.src(config.files)
    .pipe(concat(config.file))
    .pipe(gulp.dest(config.dest));
});