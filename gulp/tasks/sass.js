'use strict';

var gulp = require('gulp'),
  connect = require('gulp-connect'),
  sass = require('gulp-sass'),
  config = require('./../config')('sass');

gulp.task('sass', function () {
  gulp.src(config.files)
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest(config.dest))
    .pipe(connect.reload());
});
