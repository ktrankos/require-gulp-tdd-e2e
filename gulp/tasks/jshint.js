'use strict'
var gulp = require('gulp')
, jshint = require('gulp-jshint')
, config = require('./../config')('jshint');

gulp.task('jshint', function() {
  gulp.src(config.src)
  .pipe(jshint())
  .pipe(jshint.reporter(config.reporter));
});