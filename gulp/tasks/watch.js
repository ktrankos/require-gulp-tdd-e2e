'use strict'

var gulp = require('gulp')
, connect = require('gulp-connect')
, Browseryfy = require('./browserify')
, Html = require('./html')
, Sass = require('./sass')
, browserify = require('./../config')('browserify')
, html = require('./../config')('html')
, sass = require('./../config')('sass');

gulp.task('watch', function () {
    gulp.watch(html.files, ['html']);
    gulp.watch(browserify.watchFiles, ['browserify'])  
    gulp.watch(sass.files, ['sass'])  
});
 