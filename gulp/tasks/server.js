'use strict';

var gulp = require('gulp'),
  connect = require('gulp-connect'),
  config = require('./../config')('connect');

gulp.task('connect', function () {
    connect.server({
        root: config.root,
        livereload: true
    });
});