'use strict';

var gulp = require('gulp'),
  browserify = require('gulp-browserify'),
  concat = require('gulp-concat'),
  connect = require('gulp-connect'),
  config = require('./../config')('browserify');


gulp.task('browserify', function() {
  gulp.src(config.files)
  .pipe(browserify({
    insertGlobals: config.insertGlobals,
    debug: config.debug
  }))
  .pipe(concat(config.file))
  .pipe(gulp.dest(config.dest))
  .pipe(connect.reload());
});

/*gulp.task('browserify-watch',  function() {  
  gulp.watch(
    browserify.watchFiles,['jshint','browserify-create']
  );
  
});*/