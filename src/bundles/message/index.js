'use strict';

var singletonMessages = require('./factory/singleton.factory');
var messageController = require('./controller/message.controller');
var messageRun = require('./run/message.run');
var messageDirective = require('./directive/message.directive');
var messageFactory = require('./factory/message.factory');

angular.module('ats-message', [])
  .run(messageRun)
  .factory('Message', messageFactory)
  .factory('$singleton', singletonMessages)
  .directive('atsMessage', messageDirective)
  .controller('messageController', messageController);

singletonMessages.$inject = ['$window'];
messageDirective.$inject = ['Message', '$interval']
messageFactory.$inject= ['$singleton'];

