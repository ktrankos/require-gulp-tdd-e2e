'use strict';

module.exports = function singletonFactory($window){
  
  var Singleton =(function(){

    var instance = null;
    function init(){
      var elms=0, positions=[], messages=[];
      return {
        addMessage: function(message){
          messages.push(message);
          elms++;
        },
        getMessages: function(){
          return elms;
        },
        addPosition: function(id, position, width){
          positions.push({
            id: id,
            position:position,
            width: width
          });
          return positions;
        },
        getPositions: function(){
          
          return positions;
        },
        removeMessageById: function(id){
          var i=0, l=positions.length;
          for(i; i<l; i++)
            if(positions[i].id===id) break;
          messages.splice(i, 1);
          positions.splice(i, 1);
          return messages;
        }
      };
    }
    return {
      getInstance: function(){
        if(instance===null) instance = init();
        return instance;
      }
    };
  })();
  return Singleton;
};