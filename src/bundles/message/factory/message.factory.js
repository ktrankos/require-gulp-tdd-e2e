'use strict';

module.exports = function messageFactory($singleton){
  var singleton = $singleton.getInstance();
  var Message = function(options){
      this.defaults = {title: 'estamos locos!!',  margin: 10,
        animated: true, animationEntry: 'fadeIn'};
      this.element = null;
      angular.merge(this.defaults, options);

      /**
       * [añade el attr id al element] 
       * @param  {[DOMElement]} el  [Dom element de la directiva]
       * @param  {[int]} id [valor de id] 
       * @return {[DOMElement]}    [el]
       */
      function _addAttrIdToElement(el, id){
        el.attr('id', 'ats-message-'+id);
        return el;
      }
      /**
       * [añade mensajes [this] al singleton]
       * @param  {[this]} obj [El propio objeto message.factory this]
       * @return {[this]}
       */
      function _addMessage(obj){
        singleton.addMessage(obj);
        return obj;
      }
      /**
       * [añade las class al DOMElement de la directiva]
       * @param  {[DOMElement]} el  [Dom element de la directiva]
       * @param {[array]} classes [array de clases(string)]
       */
      function _addClassesToElement(el, classes){
        var i = 0, l = classes.length;
        for(i; i<l; i++)
          el.addClass(classes[i]);
        return el;
      }
      /**
       * [añade las propiedades de style al DOMElement de la directiva]
       * @param  {[DOMElement]} el  [DOMElement de la directiva]
       * @param {[string]} position [posicion el element definida en la directiva tp|tl|br|bl]
       * @param {[object]} coors    [Coordenadas obtenidas por _getCoors {x, y}]
       */
      function _addStyleToElement(el, position, coors){     
        
        var cssAttrs = {
          position: 'absolute'
        };
        if(position==='br' || position==='tr')
          cssAttrs.right = coors.x
        else 
          cssAttrs.left = coors.x

        if(position==='tl' || position==='tr')
          cssAttrs.top = 0;
        else
          cssAttrs.bottom=0;

        el.css(cssAttrs);
      }
      /**
       * [calculo de la posicion x e y del element]
       * @param  {[string]} position [valores dados por la directiva tl|tr|bl|br]
       * @param  {[int]} id       [contador de elemento controlar el repintado]
       * @return {[object]}       [valores x, y]
       */
      function _getCoors(position, margin, id){
        var x = 0,
        i=0,
        positions = singleton.getPositions(), 
        l = (typeof(id)==='undefined')? positions.length : id;
        for(i;i<l;i++)
          if(position===positions[i].position)
            x+=positions[i].width+margin;

        return {
          x: x
        };
      }

      function _init(obj, element, type, defaults){
        defaults.id = singleton.getMessages();     
        _addAttrIdToElement(element, defaults.id);
        _addMessage(obj);
        var classes = ['ats-message', type];
        if(defaults.animated) {
          classes.push('animated');
          classes.push(defaults.animationEntry);
        }
        _addClassesToElement(element, classes);
        var coors = _getCoors(defaults.position, defaults.margin);
        _addStyleToElement(element, defaults.position, coors);
        singleton.addPosition(defaults.id, defaults.position, element[0].offsetWidth);
      }
      
      this.removeMessage = function(element, animation){        
        _addClassesToElement(element, [animation]);
        var messagesUpdate = singleton.removeMessageById(this.defaults.id);
        var i=0, l= messagesUpdate.length;
        for(i;i<l;i++) messagesUpdate[i].update(i);
        return this;
      };
      this.update = function(position){   
        var coors = _getCoors(this.defaults.position, this.defaults.margin, position);        
        _addStyleToElement(this.element, this.defaults.position, coors);
        return this;
      };
      this.Alert = function(element){
        this.element = element;
        _init(this, element, 'alert', this.defaults);
        return this;
      };
      this.Warm = function (element){
        this.element = element;
        _init(this, element, 'warm', this.defaults);
        return this;
      };
      this.Error = function (){
        this.type = 'error';
        return this;
      };
      this.Debug = function (){
        this.type = 'debug';
        return this;
      };

      return this;
  };
  return Message;
};