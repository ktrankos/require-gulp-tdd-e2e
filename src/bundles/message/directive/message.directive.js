'use strict';

module.exports = function messageDirective(Message, $interval){


  return{
    compile: compile,
    restrict: 'AE',
    scope:{
      text:'@',
      type:'@',
      position:'@',
      duration: '@',
      animationEntry:'@'
    }
  };

  function compile(element, attrs){

    return {
      pre: preLink,
      post: postLink
    };
  }

  function preLink (scope, element, attrs){
    var vars = {};
    if(scope.title) vars.title = scope.title;
    if(scope.duration) vars.duration = scope.duration;
    if(scope.position) vars.position = scope.position;
    if(scope.animationEntry) vars.animationEntry = scope.animationEntry;
    element.html(scope.text);
    switch(scope.type.toLowerCase()){
      case  'alert':
        attrs.message = new Message(vars).Alert(element);
      break;
      case  'warm':
        attrs.message = new Message(vars).Warm(element);
      break;
    }
    A(); B();
  }
  function A(){
    console.log('A');
  }
  function B(){
    console.log('B');
  }
  function postLink (scope, element, attrs){

    var duration = parseInt(scope.duration);
    setTimeout(function(){
      attrs.message.removeMessage(element, 'fadeOut');
    }, duration);
  }
};
