'use strict'
module.exports = function DemoController($scope, $interval, $http, Car){
  var vm = this;
  vm.count = 0;
  vm.value  = 'soy demo';
  vm.fight = fight;


  vm.loadJSON = 
  vm.tryIt = function(){
    return 'prueba la grati de mi partes';
  };

  vm.car = new Car('blue');
  vm.messages = [
    {type: 'alert', duration: 9200, text: 'primero de todos los que estais aqui so male', position: 'br'},
    {type: 'warm', duration: 2200, text: 'Segundo y mas importante', position: 'br'}
  ];
  vm.alert = {title: 'me lo cambio de sitio'};
  vm.message = {title:'Pre'};
  var stop;
  var Foo = function(){
    var privateVar = "buz";
    function privateMethod(){ return privateVar; };
    this.bar = privateMethod();
  };

  var fooFactory = {
    create: function(){ return new Foo(); }
  };

  var f = fooFactory.create();
  var fight = function() {
    if ( angular.isDefined(stop) ) return;
      stop = $interval(function() {
        if (vm.count<50) {
          vm.count++;
        } else {
          $interval.cancel(stop);
          stop = false;
        }
      }, 700);
   };
  fight();
};
