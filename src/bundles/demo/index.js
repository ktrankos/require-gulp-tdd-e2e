var demoCtrl = require('./controller/demo.ctrl');
var buttonDirective = require('./directive/button.directive');
var carFactory = require('./factory/car.factory');


angular.module('myApp')
.directive('buttonDirective', buttonDirective)
.controller('demoCtrl', demoCtrl)
.factory('Car', carFactory);

demoCtrl.$inject = ['$scope', '$interval', '$http', 'Car'];
