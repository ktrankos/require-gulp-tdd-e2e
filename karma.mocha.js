// Karma configuration
var istanbul = require('browserify-istanbul');

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: './',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'chai', 'browserify'],


    // list of files / patterns to load in the browser
    //TODO: optimizar este listado con patterns, mmmm
    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'www/directive/*.html',
      'src/*.js',
      'src/*/**/*.js',
      'test/tdd/*.test.js'
    ],
    browserify: {
      debug: true,
      transform: ['browserify-istanbul'],
      extensions: ['.js']
    },

    exclude: [
    ],
    coverageReporter: {

      dir : 'coverage/',
      reporters:[
        {type: 'lcov', dir:'coverage/'},
        {type: 'teamcity'},
        {type: 'text-summary'}
      ]
    },

    preprocessors: {
      'www/directive/*.html': ['ng-html2js'],
      'src/*.js':['browserify'],
      'src/*/**/*.js': ['browserify']
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'www/',
    },

    // test results reºorter to use
    // possible values: 'dots', 'progress', 'spec'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['coverage', 'mocha'],

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these ºsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,
    junitReporter : {
      outputFile: 'unit.xml',
      suite: 'unit'
    }
  });
};
